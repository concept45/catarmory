<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Spell {

	protected $db;
	protected $dbh;

	private $_spell;

	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;
		$get_spell = $this->dbh->prepare('
			SELECT ds.`col_0` AS id,ds.`col_21` AS name,ds.`col_22` AS rank,ds.`col_23` AS description,ds.`col_24` AS tooltip,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `dbc_spell` AS ds
			LEFT JOIN `dbc_spellicon` AS dsi ON (ds.`col_19`=dsi.col_0)
			WHERE ds.`col_0`=?');
		$get_spell->execute(array($id));
		$this->_spell = $get_spell->fetch(PDO::FETCH_ASSOC);
	}


	/**
	 * Returns spell informations
	 * @return array spell informations
	 */
	public function get_spell() {
		if (!$this->_spell['id'])
			return;

		return $this->_spell;
	}

	/**
	 * Returns spell name
	 * @return string spell name
	 */
	public function get_name() {
		return $this->_spell['name'];
	}

}

/*

game/DataStores/DBCStructure.h

SELECT ds.`col_0` AS id,ds.`col_21` AS name,ds.`col_22` AS rank,ds.`col_23` AS description,ds.`col_24` AS tooltip,dsi.`col_1` AS icon,
dsp.`col_1` AS manaCost,dsp.`col_2` AS manaCostPerlevel,dsp.`col_3` AS ManaCostPercentage,dsp.`col_7` AS ManaCostPercentageFloat
FROM `dbc_spell` AS ds
LEFT JOIN `dbc_spellicon` AS dsi ON (ds.`col_19`=dsi.`col_0`)
LEFT JOIN `dbc_spellpower` AS dsp ON (ds.`col_42`=dsp.`col_0`)
WHERE ds.`col_0`=48045
*/