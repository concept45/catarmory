<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class ItemEnchants {
	protected $db;
	protected $dbh;
	private $_enchants = array();

	function __construct($db) {
		$this->db = $db;
		$this->dbh = $db->dbh;
	}

	// index 0 - spellitemenchant id
	// index 1 - ?charges?
	// index 2 - ?charges?
	// index 3 - name


	function parse($string) {
		$raw_enchantments = explode(' ',$string);
		for ($i=0;$i<=14;++$i) {
			$this->_enchants[$i] = array();
			for ($j=0;$j<=2;++$j) {
				$this->_enchants[$i][$j] = $raw_enchantments[$i*3+$j];		# fix Undefined offset
				if ($j == 0) {
					if ($raw_enchantments[$i*3+$j] != 0) {	# fix Undefined offset
						$enchant = new SpellItemEnchantment($this->db,$raw_enchantments[$i*3+$j]);
						$this->_enchants[$i][3] = $enchant->get_name();
					}
				}
			}
		}
	}

	public function get_item_enchant($what) {
		return $this->_enchants[$what];
	}


/*
    PERM_ENCHANTMENT_SLOT           = 0,
    TEMP_ENCHANTMENT_SLOT           = 1,
    SOCK_ENCHANTMENT_SLOT           = 2,
    SOCK_ENCHANTMENT_SLOT_2         = 3,
    SOCK_ENCHANTMENT_SLOT_3         = 4,
    BONUS_ENCHANTMENT_SLOT          = 5,
    PRISMATIC_ENCHANTMENT_SLOT      = 6,                    // added at apply special permanent enchantment
    //TODO: 7,
    REFORGE_ENCHANTMENT_SLOT        = 8,
    TRANSMOGRIFY_ENCHANTMENT_SLOT   = 9,
    MAX_INSPECTED_ENCHANTMENT_SLOT  = 10,
    PROP_ENCHANTMENT_SLOT_0         = 10,                   // used with RandomSuffix
    PROP_ENCHANTMENT_SLOT_1         = 11,                   // used with RandomSuffix
    PROP_ENCHANTMENT_SLOT_2         = 12,                   // used with RandomSuffix and RandomProperty
    PROP_ENCHANTMENT_SLOT_3         = 13,                   // used with RandomProperty
    PROP_ENCHANTMENT_SLOT_4         = 14,                   // used with RandomProperty

*/



}


