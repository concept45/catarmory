<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class SpellItemEnchantment {

	protected $db;
	protected $dbh;

	private $_enchant;

	/**
	 * @param PDO database handler
	 * @param integer spellitemenchantment id
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;
		$get_enchant = $this->dbh->prepare('SELECT `col_0` AS id,`col_14` AS name,`col_17` AS gemid FROM `dbc_spellitemenchantment` WHERE `col_0`=?');
		$get_enchant->execute(array($id));
		$this->_enchant = $get_enchant->fetch(PDO::FETCH_ASSOC);
		
		if ($this->_enchant['gemid'] > 0) {
			$get_gem = $this->dbh->prepare('SELECT dis.`col_99` AS name,dg.`col_4` AS color
			FROM `db2_item_sparse` AS dis
			LEFT JOIN `dbc_gemproperties` AS dg ON (dis.`col_125`=dg.col_0)
			WHERE dis.`col_0`=?');

			$get_gem->execute(array($this->_enchant['gemid']));
			$this->_enchant['gem'] = $get_gem->fetch(PDO::FETCH_ASSOC);
		}
	}

	/**
	 * Returns enchantment name
	 * @return string enchantment name
	 */
	public function get_name() {
		return $this->_enchant['name'];
	}

	/**
	 * Returns gem color bitmask if enchant is gem)
	 * @return integer gem color bitmask
	 */
	public function get_gem_color() {
		return $this->_enchant['gem']['color'];
	}
	
}
