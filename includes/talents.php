<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Talents extends Cache {

	protected $db;
	protected $dbh;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
		$this->dbh = $db->dbh;
	}
	
	/**
	 * Get
	 * @return array
	 */
	public function get_talents_tabs($class) {
		// add caching here

		$get_tabs = $this->dbh->prepare('
			SELECT dtt.`col_0` AS id,dtt.`col_1` AS tab,dtt.`col_2` AS tier,dtt.`col_3` AS col,dtt.`col_4` AS s1,dtt.`col_5` AS s2,dtt.`col_6` AS s3,dtt.`col_7` AS s4,dtt.`col_8` AS s5,dtt.`col_9` AS r1,dtt.`col_10` AS r2,dtt.`col_11` AS r3,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `dbc_talenttab` as tt
			LEFT JOIN `dbc_talent` AS dtt ON (tt.`col_0`=dtt.`col_1`)
			LEFT JOIN `dbc_spell` AS ds ON (dtt.`col_4`=ds.`col_0`)
			LEFT JOIN `dbc_spellicon` AS dsi ON (ds.`col_19`=dsi.`col_0`)
			WHERE tt.`col_3`= 1 << ?');
		$get_tabs->execute(array($class-1));
		$tabs = array();
		foreach ($get_tabs->fetchAll(PDO::FETCH_ASSOC) as $t) {
			$max = 0;
			for ($i=1;$i<=5;++$i) {
				if ($t['s'.$i] > 0) {
					++$max;
				}
			}
			$tabs['tab_'.$t['tab']][] = array( 'id' => $t['id'],'tier' => $t['tier'], 'col' => $t['col'], 'requires' => $t['r1'] | $t['r2'] | $t['r3'], 'icon' => $t['icon'], 'max' => $max, 'spell' => $t['s1']);
		}

		return $tabs;
	}


	
}

