<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Glyph extends Cache {

	protected $db;
	protected $dbh;

	private $_glyph;

	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found.
		if ($this->_glyph = $this->get_cache(array('glyph',$id),GENERAL_DBC_EXPIRE)) {
			return;
		}

		$get_glyph = $this->dbh->prepare('
			SELECT dgp.`col_0` AS id,dgp.`col_1` AS spell_id,ds.`col_21` AS name,dgp.`col_2` AS glyph_type,REPLACE(LOWER(dsi.`col_1`),"interface\\\\spellbook\\\\","") AS icon
			FROM `dbc_glyphproperties` AS dgp
			LEFT JOIN `dbc_spell` AS ds ON (dgp.`col_1`=ds.col_0)
			LEFT JOIN `dbc_spellicon` AS dsi ON (dgp.`col_3`=dsi.col_0)
			WHERE dgp.`col_0`=?');
		$get_glyph->execute(array($id));

		if ($get_glyph->rowCount() == 1) {
			$this->_glyph = $get_glyph->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('glyph',$id),$this->_glyph);
		}
	}


	/**
	 * Returns glyph informations
	 * @return array glyph informations
	 */
	public function get_glyph() {
		if (!$this->_glyph['id'])
			return;

		return $this->_glyph;
	}

	/**
	 * Returns glyph name
	 * @return string glyph name
	 */
	public function get_name() {
		return $this->_glyph['name'];
	}
}
