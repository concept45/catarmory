<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Npc extends Cache {

	protected $npc;
	protected $db;
	protected $dbh;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of arena team
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found.
		if ($this->npc = $this->get_cache(array('npc',$id),NPC_EXPIRE)) {
			return;
		}
		
		$get_npc = $this->dbh->prepare('
			SELECT ct.`entry`,ct.`name`,ct.`faction_A`,ct.`faction_H`,ct.`npcflag`,ct.`unit_flags`,ct.`unit_flags2`,ct.`family`
			FROM `'.$this->db->worlddb.'`.`creature_template` AS ct
			WHERE ct.`entry` = ?');
		$get_npc->execute(array($id));

		if ($get_npc->rowCount() == 1) {
			$this->npc = $get_npc->fetch(PDO::FETCH_ASSOC);
			
			$quests = new Quests($this->db);
			$this->npc['starter'] = $quests->search_by_npc_starter($id);
			$this->npc['ender'] = $quests->search_by_npc_ender($id);

			$this->store_cache(array('npc',$id),$this->npc);
		}
	}

	/**
	 * Returns npc informations
	 * @return array npc informations
	 */
	public function get_npc() {
		if (!$this->npc['entry'])
			return;

		return $this->npc;
	}

	/**
	 * Returns npc name
	 * @return string npc name
	 */
	public function get_name() {
		return $this->npc['name'];
	}

}
