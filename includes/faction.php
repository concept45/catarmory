<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Faction extends Cache {

	protected $faction;
	protected $db;
	protected $dbh;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of arena faction
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found. As this is static dbc data, always return cached
		if ($this->faction = $this->get_cache(array('faction',$id),null)) {
			return;
		}
		
		$get_faction = $this->dbh->prepare('
			SELECT `col_0` AS id,`col_23` AS name
			FROM `dbc_faction`
			WHERE `col_0` = ?');
		$get_faction->execute(array($id));

		if ($get_faction->rowCount() == 1) {
			$this->faction = $get_faction->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('faction',$id),$this->faction);
		}
	}

	/**
	 * Returns faction informations
	 * @return array faction informations
	 */
	public function get_faction() {
		if (!$this->faction['id'])
			return;

		return $this->faction;
	}
	
	/**
	 * Returns faction name
	 * @return integer faction name
	 */
	public function get_name() {
		return $this->faction['name'];
	}
	
}
