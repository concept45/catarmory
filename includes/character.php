<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Character extends Cache {

	protected $char;
	protected $db;
	protected $dbh;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of arena team
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found.
		if ($this->char = $this->get_cache(array('char',$id),CHAR_EXPIRE)) {
			return;
		}

		if (intval($id) != 0) {
			$get_char = $this->dbh->prepare('
				SELECT ch.`guid`,ch.`account`,ch.`name`,ch.`race`,ch.`class`,ch.`level`,ch.`money`,ch.`health`,ch.`power1`,ch.`power2`,ch.`power3`,ch.`power4`,ch.`power5`,ch.`totalKills`,ch.`todayKills`,ch.`yesterdayKills`,chg.guildid,chg.name AS guildName,chgr.rname AS guildRank
				FROM `'.$this->db->characterdb.'`.`characters` AS ch
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_member` AS chgm ON (ch.`guid`=chgm.`guid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild` AS chg ON (chgm.`guildid`=chg.`guildid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_rank` AS chgr ON (chgm.`guildid`=chgr.`guildid` AND chgm.`rank`=chgr.`rid`)
				WHERE ch.`guid`=?');
			$get_char->execute(array($id));
		} else if (is_string($id)) {
			$get_char = $this->dbh->prepare('
 				SELECT ch.`guid`,ch.`account`,ch.`name`,ch.`race`,ch.`class`,ch.`level`,ch.`money`,ch.`health`,ch.`power1`,ch.`power2`,ch.`power3`,ch.`power4`,ch.`power5`,ch.`totalKills`,ch.`todayKills`,ch.`yesterdayKills`,chg.guildid,chg.name AS guildName,chgr.rname AS guildRank
				FROM `'.$this->db->characterdb.'`.`characters` AS ch
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_member` AS chgm ON (ch.`guid`=chgm.`guid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild` AS chg ON (chgm.`guildid`=chg.`guildid`)
				LEFT JOIN `'.$this->db->characterdb.'`.`guild_rank` AS chgr ON (chgm.`guildid`=chgr.`guildid` AND chgm.`rank`=chgr.`rid`)
				WHERE ch.`name` COLLATE utf8_general_ci = ?');
			$get_char->execute(array($id));
		} else {
			/// ???
		}

		if ($get_char->rowCount() == 1) {
			$this->char = $get_char->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('char',$id),$this->char);
		}
	}

	/**
	 * Returns character name
	 * @return string character name
	 */
	public function get_char() {
		if (!$this->char['guid'])
			return;

		return $this->char;
	}

	/**
	 * Returns character guid
	 * @return integer character guid
	 */
	public function get_guid() {
		if (!$this->char['guid'])
			return;

		return $this->char['guid'];
	}

	/**
	 * Gets character's inventory: Equipped items and items in bags and bank
	 * @return array character's inventory
	 */
	public function get_char_items() {
		if (!$this->char['guid'])
			return;

		if ($items = $this->get_cache(array('char_items',$this->char['guid']),CHAR_EXPIRE)) {
			return $items;
		}

		$get_inventory = $this->dbh->prepare('
			SELECT ci.`bag`,ci.`slot`,ci.`item` AS guid,ii.`itemEntry`,ii.`count`,dis.`col_99` AS name,dis.`col_1` AS quality,dis.`col_12` AS itemLevel,dis.`col_9` AS inventoryType,dis.col_23 AS ContainerSlots,LOWER(idi.`col_5`) AS icon
			FROM `'.$this->db->characterdb.'`.character_inventory AS ci
			LEFT JOIN `'.$this->db->characterdb.'`.item_instance AS ii ON (ci.`item`=ii.`guid`)
			LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
			LEFT JOIN `db2_item` AS di ON (ii.`itemEntry`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE ci.`guid` = ?');
		$get_inventory->execute(array($this->char['guid']));
		$items = $get_inventory->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('char_items',$this->char['guid']),$items);
		return $items;
	}

	/**
	 * Gets character's learned skills (professions, secondary skills, riding, language, weapon skills, etc). 
	 * @return array character's skills
	 */
	public function get_char_skills() {
		if (!$this->char['guid'])
			return;

		if ($skills = $this->get_cache(array('char_skills',$this->char['guid']),CHAR_EXPIRE)) {
			return $skills;
		}

		$get_skills = $this->dbh->prepare('
			SELECT cs.`skill`,cs.`value`,cs.`max`,dsl.`col_1` AS grp,dsl.`col_2` AS name,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `'.$this->db->characterdb.'`.`character_skills` AS cs
			LEFT JOIN `dbc_skillline` as dsl ON (cs.`skill`=dsl.`col_0`)
			LEFT JOIN `dbc_spellicon` AS dsi ON (dsl.`col_4`=dsi.col_0)
			WHERE cs.guid=?');
		$get_skills->execute(array($this->char['guid']));
		$skills = $get_skills->fetchAll(PDO::FETCH_ASSOC);
		$this->store_cache(array('char_skills',$this->char['guid']),$skills);
		return $skills;
	}

	/**
	 * Gets character's talents (primary and secondary) and talents distribution
	 * @return array character's inventory
	 */
	public function get_char_talents() {
		if (!$this->char['guid'])
			return;

		if ($talents = $this->get_cache(array('char_talents',$this->char['guid']),CHAR_EXPIRE)) {
			return $talents;
		}
			
		$get_talent_tabs = $this->dbh->prepare('
			SELECT tt.`col_0` AS tree_id,tt.`col_5` AS tree_index,tt.`col_6` AS tree_name,REPLACE(LOWER(si.`col_1`),"interface\\\\icons\\\\","") AS icon
			FROM `dbc_talenttab` as tt
			LEFT JOIN `dbc_spellicon` as si ON (tt.`col_2` = si.`col_0`)
			WHERE tt.`col_3`= 1 << ?');
		$get_talent_tabs->execute(array($this->char['class']-1));
		$tabs = $get_talent_tabs->fetchAll(PDO::FETCH_ASSOC);

		$get_glyphs = $this->dbh->prepare('
			SELECT `spec`,`glyph1`,`glyph2`,`glyph3`,`glyph4`,`glyph5`,`glyph6`,`glyph7`,`glyph8`,`glyph9`
			FROM `'.$this->db->characterdb.'`.`character_glyphs`
			WHERE `guid`=?');
                $get_glyphs->execute(array($this->char['guid']));
                $glyphs = array( 'primary' => array(), 'secondary' => array());
		foreach ($get_glyphs->fetchAll(PDO::FETCH_ASSOC) as $g) {
			if ($g['spec'] == 0) {
				$spec = 'primary';
			} else {
				$spec = 'secondary';
			}
			for ($i=1;$i<=9;++$i) {
				if ($g['glyph'.$i] > 0) {
					$glyph = new Glyph($this->db,$g['glyph'.$i]);
					$glyphs[$spec][] = $glyph->get_glyph();
				}
			}
		}
			
		$get_talents = $this->dbh->prepare('
			SELECT dt.`col_1` AS grp,dt.`col_2` AS tier,dt.`col_3` AS col,ct.spec,CASE ct.`spell` WHEN dt.`col_4` THEN "1" WHEN dt.`col_5` THEN "2" WHEN dt.`col_6` THEN "3" WHEN dt.`col_7` THEN "4" WHEN dt.`col_8` THEN "5" END AS points,ct.`spell`
			FROM `'.$this->db->characterdb.'`.`character_talent` AS ct 
			LEFT JOIN `dbc_talent` AS dt ON (ct.`spell` IN (dt.`col_4`,dt.`col_5`,dt.`col_6`,dt.`col_7`,dt.`col_8`))
			WHERE ct.`guid`=?');
		$get_talents->execute(array($this->char['guid']));
		$talents = array('tabs' => $tabs, 'talents' => $get_talents->fetchAll(PDO::FETCH_ASSOC), 'glyphs' => $glyphs);
		$this->store_cache(array('char_talents',$this->char['guid']),$talents);
		return $talents;

	}




	/**
	* Gets character's completed quests in zone
	* @return array character's inventory
	*/
	public function get_completed_quests($zone) {
		if (!$this->char['guid'])
			return;

		$quests = array();
		$get_quests = $this->dbh->prepare('
			SELECT qt.`Id`
			FROM `'.$this->db->characterdb.'`.`character_queststatus_rewarded` AS cqr
			LEFT JOIN `'.$this->db->worlddb.'`.`quest_template` AS qt ON (qt.`Id`=cqr.`quest`)
			WHERE qt.`ZoneOrSort` = ? AND cqr.`guid` = ?');            // no limitation
		$get_quests->execute(array($zone,$this->get_guid()));
		foreach ($get_quests->fetchAll(PDO::FETCH_ASSOC) as $q) {
			$quests[] = $q['Id'];
		}
		return $quests;
        }





}
