<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


include "configs/config.php";
include "includes/database.php";
include "includes/cache.php";
include "includes/item.php";
include "includes/itemenchants.php";
include "includes/itemset.php";
include "includes/spellitemenchantment.php";
include "includes/spell.php";
include "includes/tooltip.php";




class Tooltip {

	public $error_code = 0;
	public $error_text = '';
	public $output = array();	// output before json encoding

	protected $dbh;			// database handler

	private $_callback = '';	// jsonp callback

	private $_guid;
	private $_name;
	private $_race;
	private $_class;
	private $_level;

	function __construct() {

	}

	public function init() {
		$this->_callback = $_GET['callback'];
		$this->db = new Database();

		if ($this->db->error) {
			$this->_throw_error(410,"database error");
		} else {
			$this->data = new TooltipData($this->db);

			switch ($_GET['action']) {
				case 'get_item':
					$this->_output = $this->data->get_item((array_key_exists('guid',$_GET) ? $_GET['guid'] : NULL),(array_key_exists('entry',$_GET) ? $_GET['entry'] : NULL));
					break;
				case 'get_spell':
					$this->_output = $this->data->get_spell($_GET['entry']);
					break;
			}
		}

		$this->output();
	}

	private function _throw_error($code,$text) {
		$this->error_code = $code;
		$this->error_text = $text;
	}

	private function output() {
		if ($this->error_code != 0) {
			$error = array(
				'code' => $this->error_code,
				'text' => $this->error_text
			);
			print $this->_callback.'('. json_encode(array('error' => $error)) .');';
		} else {
			$data = array(
				'data' => $this->_output
			);

			print $this->_callback.'('. json_encode($data) .');';
		}
	}



}

error_reporting(0);
header('Content-Type: text/javascript; charset=utf8');

$armory = new Tooltip();
$armory->init();
