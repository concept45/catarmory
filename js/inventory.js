/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
 /**
 * Inventory class
 * @class Inventory
 * @singleton
 */
 
Inventory = (function(c) {
	var Inventory = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Inventory.prototype, {
		/**
		 * Initialize character skills
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.equipped = [];
			this.bags = ['backpack'];
			this.bagsItems = { 'backpack': [] };
			this.bankBags = ['bank'];
			this.bankItems = { 'bank': [] };
			// fetch from server
			this.retrieveInventory();
		},

		/**
		 * Get skills from server and render them. This means rendering equipped items on first tab, bags items on 2nd tab and bank items on 3rd tab
		 */
		retrieveInventory: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'inventory',
					guid: this.character.getGuid()
				}, function(response) {
		
					var v = Main.evaluate(response);

					for (var i in v) {
						var item = new Item(v[i]);
						item.$el.tooltip();

						if (v[i].bag == 0) {		// equipped, backpack and main bank
							if (v[i].slot <= 18) {
								item.isEquipped = true;
								this.equipped[v[i].slot] = item;
							} else if (v[i].slot >= 19 && v[i].slot <= 22) {
								item.isCharacterBag = true;
								this.bags[v[i].slot] = item;
							} else if (v[i].slot >= 23 && v[i].slot <= 38) {
								item.isInBackpack = true;
								this.bagsItems['backpack'][v[i].slot] = item;
							} else if (v[i].slot >= 39 && v[i].slot <= 66) {
								item.isBank = true;
								this.bankItems['bank'][v[i].slot] = item;
							} else if (v[i].slot >= 67 && v[i].slot <= 73) {
								item.isBankBag = true;
								this.bankBags[v[i].slot] = item;
							}
						} else {					// other bags, bank bags
							if (typeof(this.bagsItems[v[i].bag]) === "undefined") {
								this.bagsItems[v[i].bag] = [];
							}
							this.bagsItems[v[i].bag][v[i].slot] = item;
							item.isInBag = true;
						}
					}

					// when fetched, render equipped items, bags and bank
					this.renderEquipped();
					this.renderBags();
					this.renderBank();
				},this
			);
		},

		/**
		 * Render inventory
		 */
		renderEquipped: function() {
			for (var i=0; i<=18; ++i) {
				if (typeof this.equipped[i] === 'undefined') {
					$('#slot_'+i).html('');
				} else {
					var item = this.equipped[i];
					$('#slot_'+i).append(item.$el);
				}
			}
		},

		/**
		 * Render bags
		 */
		renderBags: function() {
			var scope = this;
			
			// list of bags
			var $basic_pane = $('<div class="char_basic_pane"></div>');
			var $bags_wrapper = $('<div class="bags_wrapper"></div>');
			$basic_pane.append($bags_wrapper);
			var $table = $('<table class="sofT"><thead></thead><tbody><tr></tr></tbody></table>');
			var $column = $('<td></td>');
			var $backpack = $('<div class="quality_1"><a href="javascript:void(0)"><img style="box-shadow: 0px 0px 5px #ccc;" src="/icons/inv_misc_bag_08.png"></a></div>');
			$backpack.bind('click',this._openBag);
			$column.append($backpack);
			$('tr',$table).append($column); 
			for (var i=19; i<=22; ++i) {
				if (typeof this.bags[i] === 'undefined') {
					$('tr',$table).append($('<td><div><img src="images/inv_empty.png"></div></td>'));
				} else {
					var $column = $('<td></td>');
					this.bags[i].$el.bind('click',this._openBag);
					$column.append(this.bags[i].$el);
					$('tr',$table).append($column);
				}
			}
			$bags_wrapper.append($table);
			$('#char_bags').append($basic_pane);

			// bags contents
			for (var b in this.bags) {
				var guid;
				var slots;
				if (this.bags[b] == 'backpack') {
					guid = 'backpack';
					slots = 16;
					name = 'Backpack';
				} else {
					guid = this.bags[b].guid;
					slots = this.bags[b].ContainerSlots;
					name = this.bags[b].name;
				}
				var $bag = $('<div class="bags" id="bag_'+guid+'" style="display:none"><h2 style="color: black">'+slots+' slots '+name+'</h2></div>');
				var $table = $('<table class="sofT"><thead></thead><tbody></tbody></table>');
				
				for (var i=0; i<(slots/4); ++i) {
					$row = $('<tr></tr>');
					for (var j=0; j<4; ++j) {
						var slot = (i*4+j);
						if (slot < slots) {
							$column = $('<td></td>');
							if (guid == 'backpack') {
								slot += 23;	// backpack fix
							}
							if (typeof(this.bagsItems[guid]) === "undefined" || typeof(this.bagsItems[guid][slot]) === "undefined") {
								$column.append($('<div><img src="images/inv_empty.png"></div>'));
							} else {
								$column.append(this.bagsItems[guid][slot].$el);
							}
							$row.append($column);
						}
					}
					$table.append($row);
				}
				$bag.append($table);
				$bags_wrapper.append($bag); 
			}

			// bags searching
			var $char_aditional = $('<div class="char_aditional"></div>');
			var $bags_wrapper = $('<div class="bags_wrapper"><h2 style="color: black">Search in bags</h2></div>');
			$char_aditional.append($bags_wrapper);
			var $input = $('<input type="text">');
			var $results = $('<ul id="char_bags_recap"></ul>');
			$input.keyup(function(e) {
				scope._searchIn(e,'bags',$results,scope);
			});
			$bags_wrapper.append($input);
			$bags_wrapper.append($results);
			$('#char_bags').append($char_aditional);
			$('#char_bags').append($('<div class="clear"></div>'));
		},

		/**
		 * Render bank
		 * @abstract
		 */
		renderBank: function() {
			//
		},
		
		/**
		 * Opens a bag. this is an onClick callback function
		 */
		_openBag: function() {
			var guid = ($(this).attr('data-guid'));
			if (!guid)
				guid = 'backpack';

			$('#bag_'+guid).toggle();
			return false;
		},

		/**
		 * Perform search in characters bags or bank
		 * @param {Event} e keyup event
		 * @param {String} where "bank" or "bags"
		 * @param {Object} $results jQuery object  
		 * @param {Object} _scope inventory scope
		 */
		_searchIn: function(e,where,$results,_scope) {
			var what = $(e.target).val();
			$results.html('');
			var bags;
			if (where == "bags") {
				bags = _scope.bags;
			} else {
				bags = _scope.bankBags;
			}
			
			for (var i in bags) {
				var guid = bags[i];
				if (bags[i] != 'backpack' && bags[i] != 'bank') {
					guid = bags[i].guid;
				}
				for (var j in _scope.bagsItems[guid]) {
					if (_scope.bagsItems[guid][j].name.toLowerCase().match(what)) {
						$li = $('<li style="background: #333; padding: 2px;"></li>');
						$link = _scope.bagsItems[guid][j].$link;
						$link.tooltip();
						$li.append($link);
						$results.append($li);
					}
				}
			}
			
		}
		
	});

	return Inventory;
})();
