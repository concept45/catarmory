/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Npc class
 * @class Npc
 * @singleton
 */
Npc = (function(n) {
	var Npc = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Npc.prototype, {
		/**
		 * Initialize Npc
		 * @param {Object} n Npc object data from server
		 */
		init: function(n) {
			this.entry = n.entry;
			this.name = n.name;
			this.initBasic();
		},

		/**
		* Init basic stuff - render
		*/
		initBasic: function() {
			$('#npc_name').html(this.name);
		}
	});

	return Npc;
})();
  