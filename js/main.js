/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Main class
 * @class Main
 * @singleton
 */

var mainMenu = [
	{ name: 'basic', id: 'character'},
	{ name: 'bags', id: 'char_bags' },
	{ name: 'bank', id: 'char_bank' },
	{ name: 'spells', id: 'char_main_spells'},
	{ name: 'quests', id: 'char_quests'},
	{ name: 'achievements', id: 'char_achievements'},
	{ name: 'talents', id: 'char_talents_tree'}
];

var guildMenu = [
	{ name: 'basic', id: 'guild'},
	{ name: 'members', id: 'guild_members' },
];

var arenateamMenu = [
	{ name: 'basic', id: 'arenateam'},
];

var npcMenu = [
//	{ name: 'basic', key: 'basic', id: 'npc_basic'},
	{ name: 'starts', key: 'starter', id: 'npc_starter'},
	{ name: 'ends', key: 'ender', id: 'npc_ender'},
];

// Main singleton
var Main = {
	/**
	 * Initialize Application
	 * @param {string} server server name
	 * @param {string} url armory ajax PHP gateway script
	 */
	init: function(server,url) {
		this.server = server;
		this.url = url;
	},

	/**
	 * Get character data from server
	 * @param {string} charName character name
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getChar: function(charName,activeTab) {
		var tabs = new Tabs($('#ul_menu'),false);

		for (var i in mainMenu) {
			tabs.add({
				id: mainMenu[i].name,
				name: L12N(mainMenu[i].name),
				$el: $('#'+mainMenu[i].id),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited - probably set data to null?
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
		}

		tabs.render();

		tabs.activate(activeTab ? activeTab : 'basic');

		// watch for hash changes of 1st index (activate appropriate tab)
		Router.bind(1,function(tab) {
			tabs.activate(tab);
		});

		this.fetch(
			{
				what: 'char',
				action: 'basic',
				name: charName
			}, function(response) {
				// evaluate response for errors
				var c = this.evaluate(response);
				// create new character object with basic data from ajax
				this.character = new Character(c);
			}, this
		);

	},

	/**
	 * Returns ajax response data, when there is no error. Alerts error, when found. This is application level error - i.e. character was not found, etc.
	 * @param {Object} response Ajax response.
	 * @return {Object} Ajax response data
	 */
	evaluate: function(response) {
		if (response.error) {
			alert('error response: '+error.code+' ('+error.text+')');
		}
		return response.data;
	},

	/**
	 * Fetch data from PHP backend using JSONP request
	 * @param {Object} data Request data
	 * @param {Function} cb callback
	 * @param {Object} _scope Scope in which callback is called
	 */
	fetch: function(data,cb,_scope) {
		$.ajax({
			type: "GET",
			url: this.url,
			data: data,
			dataType: "jsonp",
			error: function() {
				alert('error while requesting ajax gateway');
			},
			success: function(json) {
				cb.call(_scope, json);
			}
		});
	},
	
	/**
	 * Search for string on server. It can be character, item, guild, ...
	 * @param {string} string character name
	 * @param {string} [tab] opens selected tab when search is done.
	 */
	search: function(string,tab) {
		this.fetch(
			{
				what: 'search',
				string: string
			}, function(response) {
				var s = Main.evaluate(response);
				var tabs = new Tabs($('#ul_menu'),$('#search_result'));
				var firstTab = null;
				for (var i in s) {
					if (s[i].length > 0) {
						if (!firstTab) {
							firstTab = i;
						}
						var template;
						switch (i) {
							case 'characters': template = ListView.templates.characters ; break;
							case 'items': template = ListView.templates.items ; break;
							case 'guilds': template = ListView.templates.guilds ; break;
							case 'arenateams': template = ListView.templates.arenateams ; break;
							case 'npcs': template = ListView.templates.npcs ; break;
							case 'quests': template = ListView.templates.quests ; break;
							case 'gameobjects': template = ListView.templates.gameobjects ; break;	//planned
							case 'spells': template = ListView.templates.spells ; break;	//planned
						}
						var list = new List(template,s[i]);
						tabs.add({
							id: i,
							name: L12N(i),
							data: list,
							onClick: function(obj) {
								Router.setParameter(2,obj.id);
							}
						});
					}
				};
				tabs.render();
				tabs.activate(tab ? tab : firstTab);
				
				// watch for hash changes in 2nd index - activate appropriate tab
				Router.bind(2,function(tab) {
					tabs.activate(tab);
				});
			},this
		);
	},
	
	

	/**
	 * get item data
	 * @param {Number} id Item ID
	 */
	getItem: function(id) {
		this.fetch(
			{
				what: 'item',
				id: id
			}, function(response) {
				var s = Main.evaluate(response);
				$('#item_name').text(s.name);
				$('#item_icon').html('<img src="/icons/'+s.icon+'.png">');

				$('.tooltip').attr('data-entry',id);
				$('.tooltip').attr('data-type','item');
				
				$('.tooltip').showHTML();
				
			},this
		);
	},
	

	/**
	 * Get guild from server
	 * @param {string|number} guildName guild name or guild id
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getGuild: function(guildName,activeTab) {
		var tabs = new Tabs($('#ul_menu'),false);

		for (var i in guildMenu) {
			tabs.add({
				id: guildMenu[i].name,
				name: L12N(guildMenu[i].name),
				$el: $('#'+guildMenu[i].id),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
		}

		tabs.render();

		tabs.activate(activeTab ? activeTab : 'basic');

		// watch for hash changes of 1st index (activate appropriate tab)

		Router.bind(1,function(tab) {
			tabs.activate(tab);
		});
		
		// fetch basic guild data
		this.fetch(
			{
				what: 'guild',
				action: 'basic',
				name: guildName
			}, function(response) {
				// evaluate response for errors
				var c = this.evaluate(response);
				// create new quild object with basic data from ajax
				this.guild = new Guild(c);
			}, this
		);
	},

	/**
	 * Get arenateam from server
	 * @param {string|number} teamName arenateam name or arenateam id
	 * @param {string} [activeTab] opens selected tab while character is loaded
	 */
	getArenateam: function(arenateamName,activeTab) {
		var tabs = new Tabs($('#ul_menu'),false);

		for (var i in arenateamMenu) {
			tabs.add({
				id: arenateamMenu[i].name,
				name: L12N(arenateamMenu[i].name),
				$el: $('#'+arenateamMenu[i].id),
				data: function() {
					// now, when function is passed as data, it knows that $el holds pre-generated tab content
					// this needs to be revisited
				},
				onClick: function(obj) {
					Router.setParameter(1,obj.id);
				},
				_scope: this
			});
		}

		tabs.render();

		tabs.activate(activeTab ? activeTab : 'basic');

		// watch for hash changes of 1st index (activate appropriate tab)

		Router.bind(1,function(tab) {
			tabs.activate(tab);
		});
		
		// fetch basic team data
		this.fetch(
			{
				what: 'arenateam',
				action: 'basic',
				name: arenateamName
			}, function(response) {
				// evaluate response for errors
				var c = this.evaluate(response);
				// create new arenateam object with basic data from ajax
				this.guild = new Guild(c);
			}, this
		);
	},


	/**
	 * Get quests from server
	 * @param {number} quest id
	 */
	getQuest: function(questId) {
		
		// fetch basic quest data
		this.fetch(
			{
				what: 'quest',
				id: questId
			}, function(response) {
				// evaluate response for errors
				var c = this.evaluate(response);
				// create new quest object with basic data from ajax
				this.quest = new Quest(c);
			}, this
		);
	},

	


	/**
	 * Get npcs from server
	 * @param {number} npc entry
	 */
	getNpc: function(entry,activeTab) {

		
		// fetch basic quest data
		this.fetch(
			{
				what: 'npc',
				id: entry
			}, function(response) {
				// evaluate response for errors
				var c = this.evaluate(response);
				
				
				var tabs = new Tabs($('#ul_menu'),$('#npc_result'));
				
				
				tabs.add({
					id: 'basic',
					name: L12N('basic'),
					$el: $('#npc_basic'),
					data: function() {
						// now, when function is passed as data, it knows that $el holds pre-generated tab content
						// this needs to be revisited - probably set data to null?
					},
					onClick: function(obj) {
						Router.setParameter(1,obj.id);
					},
					_scope: this
				});
				
				
				for (var i in npcMenu) {
					
					var template;
					switch (npcMenu[i].key) {
						case 'starter': template = ListView.templates.quests ; break;
						case 'ender': template = ListView.templates.quests ; break;
					}
					var list = new List(template,c[npcMenu[i].key]);
					
					
					tabs.add({
						id: npcMenu[i].name,
						name: L12N(npcMenu[i].name),
						data: list,
						onClick: function(obj) {
							Router.setParameter(1,obj.id);
						},
						_scope: this
					});
				}

				tabs.render();
				//if (activeTab) {
					tabs.activate(activeTab ? activeTab : 'basic');
				//}
				
				// watch for hash changes of 1st index (activate appropriate tab)
				Router.bind(1,function(tab) {
					tabs.activate(tab);
				});

				
				
				
				// create new npc object with basic data from ajax
				this.npc = new Npc(c);
			}, this
		);
	},


	/**
	 * get spell data
	 * @param {Number} id spell ID
	 */
	getSpell: function(id) {
		this.fetch(
			{
				what: 'spell',
				id: id
			}, function(response) {
				var s = Main.evaluate(response);
				$('#spell_name').text(s.name);
				$('#spell_icon').html('<img src="/icons/'+s.icon+'.png">');

				$('#spell_rank').text(s.rank);
				$('#spell_description').text(s.description);
				$('#spell_tooltip').text(s.tooltip);
				
				
			},this
		);
	},
	
};
