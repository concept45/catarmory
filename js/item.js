/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Item class
 * @class Item
 * @singleton
 */
Item = (function(i) {
	var Item = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Item.prototype, {
		/**
		 * Initialize item
		 * @param {Object} i item object data from server
		 */
		init: function(i) {
			this.guid = i.guid;
			this.entry = i.itemEntry;
			this.bag = i.bag;
			this.slot = i.slot;
			this.count = i.count;
			this.name = i.name;
			this.quality = i.quality;
			this.itemLevel = i.itemLevel;
			this.inventoryType = i.inventoryType;
			this.enchantments = i.enchantments;
			this.icon = i.icon;
			this.ContainerSlots = i.ContainerSlots;

			// jQuery element of item 

			/* TODO - change it to shared::DisplayItem() ... like
  			var display = new DisplayItem({
				guid: this.guid,
				icon: this.icon,
				count: this.count,
				name: this.name,
				quality: this.quality
			})
			this.$el = display.getIconWithName({
				size: 64,
			}));	// get jquery object and append it
 */
			
			this.$el = $('<div data-guid="'+this.guid+'" data-type="item" style="position: relative; width: 64px; height: 64px;"></div>');
			var $anchor = $('<a href="item.html#'+this.entry+'"></a>');
			this.$el.append($anchor);
			var $icon = $('<img style="" src="icons/'+this.icon+'.png">');
			$icon.error(function() {	// in case of any error, show questionmark
				$(this).attr('src','images/inv_misc_questionmark.png');
			});
			$anchor.append($icon);
			$anchor.append($('<img style="position: absolute; left: 0; top: 0" src="images/glow_quality_'+this.quality+'.png">'));
			if (this.count > 1)
				this.$el.append('<div style="position:absolute; top: 0; margin: 45px 0 0 50px; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black">'+this.count+'</div>');

			this.$link = $('<div data-type="item" data-guid="'+this.guid+'"><a href="item.html#'+this.entry+'"  style="color:'+ItemColors[this.quality]+'">['+this.name+']</a></div>');
		}
	});

	return Item;
})();
  