/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


 /**
 * Router class
 * @class Router
 * @singleton
 */



	
	Router = { 
	
		/**
		 * @property {Array} params
		 * Parsed parameters feched from location.hash
		 */
		params: [],
		/**
		 * @property {Array} event
		 * Array of parameters callbacks
		 */
		event: [],
		
		/**
		 * Initialize the router
		 */
		init: function(i,cb) {
			console.log('router init');
			this.params = [];//(window.location.hash.substr(1)).split("/");
			this.event = [];
			
			addListener();
			retest();
			
			this.bind(i,cb)
			
		},
		
		/**
		 * Get parameter by index
		 * @param {Number} p parameter index
		 * @return {String} parameter value
		 */
		getParameter: function(p) {
			return this.params[p];
		},
		
		/**
		 * Set parameter (or delete if not specified) and re-set the location.hash joining all parameters.
		 * After setting parameter removeListener() is called to stop launching `hashchange` event (prevents cycling).
		 * Then window.location hash is changed and `hashchange` listener again registered.
		 * @param {Number} p parameter index
		 * @param {String} v parameter value
		 */
		setParameter: function(p,v) {
			if (!v) {
				delete this.params[p];
			} else {
				this.params[p] = v;
			}
			removeListener();
			window.location.hash = this.params.join("/");
			addListener();
		},
		
	
		/**
		 * Bind callback that is launched when parameter is changed
		 * @param {Number} i parameter index
		 * @param {String} cb callback
		 */
		bind: function(i,cb) {
			console.log('bind '+i);
			this.event[i] = cb;
		},
		
	
	
	

	}
	

	/**
	 * Triggered by hashchange event. Checks if any of parameter was changed. When so, it runs function, that was previously bound by bind()
	 */
	var retest = function() {
		console.log('retest');
		var params = (window.location.hash.substr(1)).split("/");
		for (var i=0;i<params.length;++i) {
			if (params[i] != Router.getParameter(i)) {
				console.log('parameter '+i+' changed from '+Router.getParameter(i)+' to '+params[i]);
				Router.setParameter(i,params[i]);
				if (Router.event[i])
					Router.event[i](params[i]);
			}
		}
	};
	

	/**
	 * @event hashchange
	 * Register hashchange listener
	 */
	var addListener = function() {
		window.addEventListener("hashchange", retest, false);
	}

	/**
	 * @event hashchange
	 * Unregister hashchange listener
	 */
	var removeListener = function() {
		window.removeEventListener('hashchange', retest);

	}

