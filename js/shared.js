/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

var ItemColors = [
	'#9d9d9d', 	// 0 poor - grey
	'#fff',		// 1 common - white
	'#1eff00',	// 2 uncommon - green
	'#0070ff',	// 3 rare - blue
	'#a335ee',	// 4 epic - purple
	'#ff8000',	// 5 legendary - orange
	'#e6cc80',	// 6 artifact - yellow
	'#e6cc80'	// 7 aheirloom - yellow
];

var racemaskAlliance = ((1<<(1-1)) | (1<<(3-1)) | (1<<(4-1)) | (1<<(7-1)) | (1<<(11-1)) | (1<<(22-1)));
var racemaskHorde = ((1<<(2-1)) | (1<<(5-1)) | (1<<(6-1)) | (1<<(8-1)) | (1<<(9-1)) | (1<<(10-1)));
var zones = [
	[5145,36,45,3,25,4,46,279,41,2257,1,10,139,12,3430,3433,4714,4755,267,1537,4080,4815,38,33,44,4706,51,5144,3487,130,1519,8,5287,47,85,4922,1497,28,40,11], // Easter Kingdoms
	[331,16,3524,3525,148,1657,405,14,15,368,361,357,493,616,215,17,1637,1377,4709,406,440,141,3557,400,1638,1216,490,5034,618], // Kalimdor
	[3522,3483,3518,3523,3520,3703,3679,3519,3521], // Outland
    [3537,4024,4395,65,394,495,210,3711,67,4197,66], // NorthRend
    [5042,4737,4720,5736,5095,5389] // Cataclysm
];


function search_in_array(s,a) {
	return (a.indexOf(s) != -1);
}



/**
 * Money class
 * 
 * @class Money
 * @singleton
 */
Money = (function(m) {
	var Money = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Money.prototype, {
		/**
		 * Initialize Money
		 * @param {Number} m integer representation of money
		 */
		init: function(m) {
            this.gold = parseInt(m / 10000);
            var tmp = m % 10000;
            this.silver = parseInt(tmp / 100);
            this.copper = tmp % 100;
		},

		/**
		 * Return money as html with images
		 * @return guid
		 */
		getHTMLMoney: function() {
			return (this.gold ? this.gold + '<img src="images/g.gif"> ' : '') + 
			((this.silver || this.int < 100) ? this.silver + '<img src="images/s.gif"> ' : '') + 
			(this.copper ? this.copper + '<img src="images/c.gif">' : '');
		},
		
	});

	return Money;
})();


/**
 * DisplayItem class
 * 
 * @class DisplayItem
 * @singleton
 */
DisplayItem = (function(p) {
	var DisplayItem = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, DisplayItem.prototype, {
		/**
		 * Initialize DisplayItem
		 * @param {Object} p data of DisplayItem
		 */
		init: function(p) {
			this.guid = p.guid || null;
			this.entry = p.entry || null;
            this.icon = p.icon || 'inv_questionmark';
            this.name = p.name || 'unk';
            this.count = p.count || 1;
            this.quality = p.quality || 0;
		},

		/**
		 * Return jQuery object with item icon
		 * @param {Object} config Icon configuration
		 * @param {Number} [config.size=64] Size of generated icon in px (values are 14, 32 and 64). 
		 * @param {Boolean} [config.hideCount=false] Hide item count inside icon in bottom right corner? False = item count is displayed
		 * @param {Boolean} [config.noAnchor=false] Do not make icon clickable? False = icon is clickable
		 * @param {Boolean} [config.noTooltip=false] Do not register mouseover and tooltip on icon? False = register tooltip
		 * @return {Object} jQuery object
		 */
		getIcon: function(p) {
			var size = p.size || 64;
			var hideCount = typeof(p.hideIconCount) !== "undefined" ? p.hideIconCount : false;
			var noAnchor = typeof(p.noIconAnchor) !== "undefined" ? p.noIconAnchor : false;
			var padding = typeof(p.padding) !== "undefined" ? p.padding : false;
			var noTooltip = typeof(p.noTooltip) !== "undefined" ? p.noTooltip : false;

			var $wrap = $('<div style="position: relative" data-type="item" data-entry="'+this.entry+'"></div>');

			var $img = $('<img style="" src="/icons_'+size+'/'+this.icon+'.png">');
			if (padding)
				$img.css({padding: padding});

			if (!noAnchor) {
				var $link = $('<a href="item.html#'+this.entry+'">');
				$link.append($img);
				$wrap.append($link);
			} else {
				$wrap.append($img);	
			}

			if (this.count > 1) {
				var $count= $('<div style="position: absolute; text-shadow: 0px 0px 1px #000; text-align: right">'+this.count+'</div>');
				$count.css({top: (size-17)+'px', width: (size-4)+'px'});
				
				
				
				
				$wrap.append($count);
			}

			if (!noTooltip)
				$wrap.tooltip();

			return $wrap;
			// <a href="item.html#'+this.requiredItems[i].itemEntry+'"></a> '+ (this.requiredItems[i].count > 1 ? '<div>'+this.requiredItems[i].count+'</div>' : '' )+
		},
		
		/**
		 * Return jQuery object with icon and name behind
		 * @param {Object} config Configuration:
		 * @param {Number} [config.size=null] Icon size of icon and line height of element that encloses item name in pixels.
		 * @param {Boolean} [config.hideLinkCount=false] Hide x# after item name? False = item with count behind name
		 * @param {Boolean} [config.noLinkAnchor=false] Do not make item name clickable? False = clickable item name
		 * @param {Boolean} [config.hideIconCount=false] Hide item count inside icon in bottom right corner? False = item count is displayed
		 * @param {Boolean} [config.noIconAnchor=false] Do not make icon clickable? False = icon is clickable
		 * @return {Object} jQuery object
		 */
		getIconWithName: function(p) {
			var size = p.size || 64;
			var hideLinkCount = typeof(p.hideLinkCount) !== "undefined" ? p.hideLinkCount : false;
			var noLinkAnchor = typeof(p.noLinkAnchor) !== "undefined" ? p.noLinkAnchor : false;
			var hideIconCount = typeof(p.hideIconCount) !== "undefined" ? p.hideIconCount : false;
			var noIconAnchor = typeof(p.noIconAnchor) !== "undefined" ? p.noIconAnchor : false;

			var $table = $('<table style="border-spacing: 0;"></table>');
			var $tr = $('<tr></tr>');
			$table.append($tr);

			var $icon = this.getIcon({
				size: size,
				padding: '0 3px 0 0',
				hideCount: hideIconCount,
				noAnchor: noIconAnchor
			});
			var $name = this.getName({
				//size: size,
				hideCount: hideLinkCount,
				noAnchor: noLinkAnchor
			});

			var $ltd = $('<td></td>');
			$ltd.append($icon);
			$tr.append($ltd);

			var $rtd = $('<td style="height: 32px"></td>');
			$rtd.append($name);
			$tr.append($rtd);

			return $table;
		},

		/**
		 * Return jQuery object with item name
		 * @param {Object} config Configuration:
		 * @param {Number} [config.size=null] Used to calculate padding top in px of element that encloses item name. Null = no padding. 
		 * @param {Boolean} [config.hideCount=false] Hide x# after item name? False = item with count behind name
		 * @param {Boolean} [config.noAnchor=false] Do not make item name clickable? False = clickable item name
		 * @param {Boolean} [config.noTooltip=false] Do not register mouseover and tooltip on item name? False = register tooltip
		 * @return {Object} jQuery object
		 */
		getName: function(p) {
			var size = p.size || null;
			var hideCount = typeof(p.hideCount) !== "undefined" ? p.hideCount : false;
			var noAnchor = typeof(p.noAnchor) !== "undefined" ? p.noAnchor : false;
			var noTooltip = typeof(p.noTooltip) !== "undefined" ? p.noTooltip : false;
			
			var $div;
			if (noAnchor) {
				$div = $('<div '+(size ? 'style="padding-top: '+(size-15)+'px"' : '')+'>'+this._makeName()+(hideCount ? '' : 'x'+this.count)+'</div>');
			} else {
				$div = $('<div '+(size ? 'style="padding-top: '+(size-15)+'px"' : '')+'data-type="item" data-entry="'+this.entry+'"><a href="item.html#'+this.entry+'">'+this._makeName()+(hideCount ? '' : '<span style="color: #fff">x'+this.count+'</span>')+'</div>');
			}
			if (!noTooltip)
				$div.tooltip();
			return $div;
		},

		/**
		 * Return HTML of item
		 * @return {String} return html of item with propper color enclosed into brackets
		 */
		_makeName: function() {
			return '<span style="color:'+ItemColors[this.quality]+'">['+this.name+']</span>';
		}

	});

	return DisplayItem;
})();

