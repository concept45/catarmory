/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Tabs class
 * @class Tabs
 * @singleton
 */
Tabs = (function($el,$da) {
	var Tabs = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Tabs.prototype, {
		/**
		 * Initialize Application
		 * @param {Object} $el jQuery UL tabs element
		 * @param {Object/Boolean} $da jQuery DIV data element. When false, tabs content is pre-generated
		 */
		init: function($el,$da) {	// jQuery 'ul' element for tabs, jQuery 'div' element for tabs data
			this.$el = $el;
			this.$el.html('');	// clear tabs
			if ($da) {
				this.$da = $da;
				this.$da.html('');	// clear data results
			} else {
				this.predefined = true;
			}
			this.tabs = [];
			this.$visible = null;
			this.tabSelected = null;
		},

		/**
		 * Add tab to tabs array
		 * @param {Object} params tab configuration
		 */
		add: function(params) {
			params.key = this.tabs.length;
			this.tabs.push(params);
		},
		
		/**
		 * Render tabs and append them to DOM
		 */
		render: function() {
			var scope = this;
			for (var i=0;i<this.tabs.length;++i) {

				var results = null;

				// generate tabs contents
				var $div;
				if (typeof(this.tabs[i].data) === "object") {
					var $table = this.tabs[i].data.render();		// all objects in tabs must have method render() which returns jQuery element... i.e. List() works like this
					results = this.tabs[i].data.getNumResults();		// all objects in tabs must also have method getNumResults(), which returns number of results
					$div = $('<div style="display: none" class="tab-data" data-value="tab_'+this.tabs[i].key+'"></div>');
					$div.append($table);
					this.$da.append($div);

				} else if (typeof(this.tabs[i].data) === "function") { // TODO: change this, function is not good way
					this.tabs[i].$el.addClass('tab-data')
					this.tabs[i].$el.attr('data-value','tab_'+this.tabs[i].key);
					//this.tabs[i].data.call()
					//alert("we have function here"+this.tabs[i].key);
				} else if (typeof(this.tabs[i].data) === "string") {
					$div = $('<div style="display: none" class="tab-data" data-value="tab_'+this.tabs[i].key+'">'+this.tabs[i].data+'</div>');		// if there is just text in data, just append it
					this.$da.append($div);
				}

				// generate tabs stickers
				var $li = $('<li data-value="tab_'+this.tabs[i].key+'">'+this.tabs[i].name+ (results ? ' ('+results+')' : '')+'</li>');
				$li.bind('click',function(e) {
					scope.activate($(this));
				});

				// append tab to DOM
				this.$el.append($li);
			}
		},

		/**
		 * Activates selected tab
		 * @param {Object/String} tab object or tab key - see _getTabByKey()
		 */
		activate: function(tab) {
			
			if (typeof(tab) !== "object") {
				var t = this._getTabById(tab);
				if (!t) {
					// trying to open non-existing tab
					//alert("there is no such tab");
					return;
				}
				tabKey = 'tab_'+t.key;
			} else {
				var tabKey = tab.attr('data-value');
			}

			if (this.$visible) {
				this.$visible.hide();
			}
			if (this.tabSelected) {
				$('li[data-value="'+this.tabSelected+'"]',this.$el).removeClass('selected');
			}

			var ttab = this._getTabByKey(tabKey);
			if (ttab.$el) {
				this.$visible = ttab.$el;
			} else {
				this.$visible = $('div.tab-data[data-value="'+tabKey+'"]',this.$da);				
			}
			

			this.$visible.show();
			$('li[data-value="'+tabKey+'"]',this.$el).addClass('selected');

			this.tabSelected = tabKey;

			var t = this._getTabByKey(this.tabSelected);
			if (t) {
				t.onClick(t);
			}
		},
	
		/**
		 * Returns tab object by tab key
		 * @param {String} tabKey from data-value="". Values are "tab_0", "tab_1", ..., "tab_n"
		 * @return {Object} this.tabs[i]
		 */
		_getTabByKey: function(tabKey) {
			for (var i in this.tabs) {
				if (tabKey == 'tab_'+this.tabs[i].key) {
					return this.tabs[i];
				}
			}
			return;
		},

		/**
		 * Returns tab object by tab id
		 * @param {String} tabId is property id from tab definition object.
		 * @return {Object} this.tabs[i]
		 */
		_getTabById: function(tabKey) {
			for (var i in this.tabs) {
				if (tabKey == this.tabs[i].id) {
					return this.tabs[i];
				}
			}
			return;
		}
	});

	return Tabs;
})();


/**
 * List class
 * @class List
 * @singleton
 */
List = (function(template,data) {
	var List = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, List.prototype, {
		/**
		 * Initialize List
		 * @param {Object} template List template
		 * @param {Object/Boolean} data List data
		 * @param {Object} params configuration parameters
		 */
		init: function(template,data,params) {	// list template, data
			this.data = data;
			this.template = template;
			if (params) {
				this.hideColumns = params.hide || [];
				this.addColumns = params.add || [];
				this.additionalData = params.additionalData || null;
			}
		},

		/**
		 * Render List and return it's jQuery object. Columns are defined in ListView.templates for each list type (i.e. items list, guilds list, arenateams list, ...) 
		 * @return {Object} jQuery object of List
		 */
		render: function() {
			var $wrapper = $('<div></div>');
			
			var $info_div = $('<div class="list-top">Number of results</div>');
			$wrapper.append($info_div);			

			var $table = $('<table class="list"></table>');
			var $thead = $('<thead></thead>');
			var $tbody = $('<tbody></tbody>');

			$table.append($thead);
			$table.append($tbody);

			var $head_tr = $('<tr></tr>');

			$thead.append($head_tr);

			var columns = [];
			for (var i in this.template.columns) {
				if ((typeof(this.template.columns[i].skipHeader) === "undefined" || !this.template.columns[i].skipHeader) && !this._isColumnHidden(this.template.columns[i].id)) {
					var $th = $('<th></th>');
					if (typeof(this.template.columns[i].colspan) !== "undefined") {
						$th.attr('colspan',this.template.columns[i].colspan);
					}
					$th.append('<div style="padding: 0 5px;">'+L12N(this.template.columns[i].name)+'</div>');
					$head_tr.append($th);
					columns.push(this.template.columns[i]);
				} else {
					if (typeof(this.template.columns[i].skipHeader) !== "undefined" && this.template.columns[i].skipHeader) {
						columns.push(this.template.columns[i]);
					} 
				}
			}
			
			for (var i in this.addColumns) {
				var $th = $('<th></th>');
				$th.append('<div style="padding: 0 5px;">'+L12N(this.addColumns[i].name)+'</div>');
				$head_tr.append($th);
				columns.push(this.addColumns[i]);
			}

			for (var i in this.data) {
				var $tr = $('<tr></tr>');
				for (var j in columns) {
					var r = this.data[i];
					var c = r[(columns[j].id ? columns[j].id : columns[j].name)];

					var text = columns[j].text(c,r,this);
					var $td = $('<td></td>');
					
					if (typeof(columns[j].style) !== "undefined") {
						$td.attr('style',columns[j].style);
					}
					
					$td.append(text);
						
					if (typeof(columns[j].align) !== "undefined") {
						$td.css('text-align',columns[j].align);
					}
					$tr.append($td);
				}
				$tbody.append($tr);
			}

			$wrapper.append($table);
			
			var $info_div = $('<div class="list-top">Number of results</div>');
			$wrapper.append($info_div);

			return $wrapper;
		},

		/**
		 * Return size of List
		 * @return {Number} List size
		 */
		getNumResults: function() {
			return this.data.length;
		},
		
		_isColumnHidden: function(id) {
			for (var i in this.hideColumns) {
				if (id == this.hideColumns[i]) {
					return true;
				}
			}
		}
	});

	return List;
})();


/**
 * ListView class - definition how Lists are displayed and values are calculated
 * @class ListView
 * @singleton
 */
ListView = {};

/**
 * Template definition how Lists are displayed and values are calculated
 * @cfg {Object} config Column configuration:
 * @cfg {String} config.id Used as key for data  
 * @cfg {String} config.name Used for header. This is localised via L12N()
 * @cfg {Number} config.colspan Number of colspans in header. You will probably always use also skipHeader 
 * @cfg {Boolean} config.skipHeader Skip this column header (in combination with colspan) to match header columns with data
 * @cfg {String} config.style CSS style for each data column
 * @cfg {String} config.align Text alignment of data (center, left, right)
 * @cfg {Function} config.text Function returning jQuery object or html, that is used as content of column. Function input are 2 parameters (actual column data, row data)
*/
ListView.templates = {
	characters: {
		columns: [
			{
				id: 'name',
				name: 'Name',
				text: function(t,d,s) {	// t is actual column data, and d is row data, s is scope back to List
					return '<a href="character.html#'+t+'/basic">'+t+'</a>';
				}
			}, {
				id: 'race',
				name: 'Race',
				align: 'center',
				text: function(t) {
					return races[t];
				}
			}, {
				id: 'class',
				name: 'Class',
				align: 'center',
				text: function(t) {
					return classes[t];
				}
			},
			{
				id: 'level',
				name: 'Level',
				align: 'center',
				text: function(t) {
					return t;
				}
			}
		]
	}, // end of characters

	items: {
		columns: [
			{
				id: 'name',
				name: 'Name',
				colspan: 2,
				style: "width: 1px; padding: 0px; border-right-style: none;",
				text: function(t,d) {	// t is actual column data, and d is row data
					var $icon = $('<a data-type="item" data-entry="'+d.entry+'" href="item.html#'+d.entry+'"><img src="/icons_32/'+d.icon+'.png"></a>');
					$icon.tooltip();
					return $icon;
				}
			}, {
				id: 'name',
				name: 'name',
				skipHeader: true,
				style: "border-left-style: none;",
				text: function(t,d) {	// t is actual column data, and d is row data
					var $item = $('<a data-type="item" data-entry="'+d.entry+'" href="item.html#'+d.entry+'">'+t+'</a>')
					$item.tooltip();
					return $item;
					
				}
			}, {
				id: 'itemLevel',
				name: 'Item_Level',
				align: 'center',
				text: function(t) {
					return t;
				}
			}, {
				id: 'requiredLevel',
				name: 'Required_Level',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'inventoryType',
				name: 'Slot',
				align: 'center',
				text: function(t) {
					return inventoryTypes[t];
				}
			}
		]
	}, // end of items
	
	guilds: {
		columns: [
			{
				id: 'guildName',
				name: 'Name',
				text: function(t,d) {	// t is actual column data, and d is row data
					return '<a href="guild.html#'+t+'/basic">'+t+'</a>';
				}
			}, {
				id: 'side',
				name: 'Side',
				align: 'center',
				text: function(t,d) {
					var faction = (1<<(d.leaderRace-1) & racemaskAlliance ? 'alliance' : 'horde');
					return '<img src="images/'+faction+'.png">';
				}
			}, {
				id: 'guildLevel',
				name: 'Guild_Level',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'members',
				name: 'Members',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'leaderName',
				name: 'Leader_Name',
				text: function(t) {
					return '<a href="character.html#'+t+'">'+t+'</a>';
				}
			}
		]
	}, // end of guilds

	arenateams: {
		columns: [
			{
				id: 'arenateamName',
				name: 'Name',
				text: function(t,d) {	// t is actual column data, and d is row data
					return '<a href="arenateam.html#'+t+'/basic">'+t+'</a>';
				}
			}, {
				id: 'side',
				name: 'Side',
				align: 'center',
				text: function(t,d) {
					var faction = (1<<(d.captainRace-1) & racemaskAlliance ? 'alliance' : 'horde');
					return '<img src="images/'+faction+'.png">';
				}
			}, {
				id: 'type',
				name: 'Type',
				align: 'center',
				text: function(t) {
					return t+'v'+t;
				}
			},
			{
				id: 'rating',
				name: 'Rating',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'rank',
				name: 'Rank',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'seasonGames',
				name: 'Season_Games',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'seasonWins',
				name: 'Season_Wins',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'weekGames',
				name: 'Week_Games',
				align: 'center',
				text: function(t) {
					return t;
				}
			},
			{
				id: 'weekWins',
				name: 'Week_Wins',
				align: 'center',
				text: function(t) {
					return t;
				}
			}
		]
	},	// end of arenateams
	
	

	quests: {
		columns: [
			{
				id: 'Title',
				name: 'Title',
				text: function(t,d) {	// t is actual column data, and d is row data
					return '<a href="quest.html#'+d.Id+'">'+t+'</a>';
				}
			}, {
				id: 'Level',
				name: 'Level',
				align: 'center',
				text: function(t) {
					return t;
				}
			}, {
				id: 'MinLevel',
				name: 'MinLevel',
				align: 'center',
				text: function(t) {
					return t;
				}
			}, {
				id: 'side',
				name: 'Side',
				align: 'center',
				text: function(t,d) {
					if (d.RequiredRaces > 0) {
						var faction = (d.RequiredRaces & racemaskAlliance ? 'alliance' : 'horde');
						return '<img src="images/'+faction+'.png">';
					} else {
						return;
					}
				}
			}, {
				id: 'Rewards',
				name: 'Rewards',
				align: 'center',
				text: function(t,d) {
					var $wrap = $('<div></div>');
					if (d.RewardItems.length > 0) {
						var $rewards = $('<div>You will receive: </div>');
						for (var i in d.RewardItems) {
							var $img = $('<a data-type="item" data-entry="'+d.RewardItems[i].itemEntry+'" href="item.html#'+d.RewardItems[i].itemEntry+'"><img style="padding-right: 5px" src="/icons_14/'+d.RewardItems[i].icon+'.png"></a>');
							$rewards.append($img);
							$img.tooltip();
						}
						$wrap.append($rewards);
					}
					
					if (d.ChooseItems.length > 0) {
						var $choose = $('<div>Pick one of: </div>');
						for (var i in d.ChooseItems) {
							var $img = $('<a data-type="item" data-entry="'+d.ChooseItems[i].itemEntry+'" href="item.html#'+d.ChooseItems[i].itemEntry+'"><img style="padding-right: 5px" src="/icons_14/'+d.ChooseItems[i].icon+'.png"></a>');
							$choose.append($img);
							$img.tooltip();
							
						}
						$wrap.append($choose);
					}
					
					return $wrap;
					
				}
			}, {
				id: 'Experiences',
				name: 'XP',
				align: 'center',
				text: function(t) {
					if (t > 0) {
						return t;
					}
				}
			}, {
				id: 'RewardOrRequiredMoney',
				name: 'Money',
				align: 'center',
				text: function(t) {
					if (t > 0) {
						var money = new Money(t);
						return money.getHTMLMoney();
					}
				}
			}, {
				id: 'ZoneOrSort',
				name: 'Zone',
				align: 'center',
				text: function(t) {
					return L12N('zone_'+t,true);
				}
			}
			
		]
	},
	
	

	npcs: {
		columns: [
			{
				id: 'name',
				name: 'Name',
				text: function(t,d) {	// t is actual column data, and d is row data
					var $wrap = $('<div></div>'); 
					var $name = $('<div><a href="npc.html#'+d.entry+'">'+t+'</a></div>');
					$wrap.append($name);
					if (d.subname) {
						var $subname = $('<div style="font-size: 12px">&lt;'+d.subname+'&gt;</div>');
						$wrap.append($subname);
					}
					return $wrap;
					
				}
			}
		]
	},	// end of npcs
	
	
};

