/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


 ;(function($) {
	var cache = {
		get_item: {},
		get_character: {},
		get_spell: {},
		get_guild: {},
		get_arenateam: {},
		get_quest: {}
	};
	var $el = null;

	function createElement() {
		if (!$el) {
			$el = $('<div style="position: absolute; width: 320px; min-height: 100px; background: black; top: 0; left: 0; display: none; color: #fff; border: 1px solid #ddd; border-radius: 5px; padding: 7px; font-family: Verdana, sans-serif; box-shadow: 0 0 1px 1px #000;"></div>');
			$('body').append($el);
		}
	};

	function showHTML(e) {
		var entry = e.attr('data-entry');
		$el = $('<div style="width: 320px; min-height: 100px; background: black; color: #fff; border: 1px solid #ddd; border-radius: 5px; padding: 7px; font-family: Verdana, sans-serif; box-shadow: 0 0 1px 1px #000;"></div>');
		fetch({
			action: 'get_item',		// TODO - also cases for characters, spells, guilds, ...
			entry: entry
		},function(data) {
			$el.html(data.data.html);
			e.append($el);
		})
		
	};
	
	function show(e) {
		var guid = $(this).attr('data-guid');
		var entry = $(this).attr('data-entry');
		var type = $(this).attr('data-type');
		var action = 'get_item';	// default when data-type is not set
		switch (type) {
			case 'item': action = 'get_item'; break;			// guid || entry
			case 'character': action = 'get_character'; break;	// guid
			case 'spell': action = 'get_spell'; break;			// entry
			case 'guild': action = 'get_guild'; break;			// guid
			case 'arenateam': action = 'get_arenateam'; break;	// guid
			case 'quest': action = 'get_quest'; break;			// entry
		}
		
		if (guid || entry) {
			fetch({
				action: action,
				guid: guid,
				entry: entry
			},function(data) {
				$el.html(data.data.html);
			})
		} else {
			$el.html('missing entry or guid');
		}
		$el.show();
		update(e);
	};

	function update(e) {
		var left = e.pageX+15;
		var top = e.pageY;
		if (top + $el[0].offsetHeight - $(window).scrollTop() + 20 >  $(window).height()) {
			top -= top + $el[0].offsetHeight - $(window).scrollTop() - $(window).height() + 20;
		}
		$el.css({
			left: left,
			top: top
		})
	}

	function hide() {
		$el.html('');
		$el.hide();
	};

	/*
	fetch - method for ajax jsonp request
	data: query parameters
	cb: callback evaluated on success
	_scope: scope in which callback will be evaluated
	*/
	function fetch(data,cb) {
		if (typeof(cache[data.action][data.guid+'_'+data.entry]) !== "undefined") {
			cb(cache[data.action][data.guid+'_'+data.entry]);
		} else {
			$.ajax({
				type: "GET",
				url: CONFIG.ajaxTooltipURL,
				data: data,
				dataType: "jsonp",
				error: function() {
					alert('error while requesting ajax gateway');
				},
				success: function(json) {
					cache[data.action][data.guid+'_'+data.entry] = json;
					cb(json);
				}
			});
		}
	};

	$.fn.extend({
		tooltip: function() {
			createElement();
			return this.each(function() {
			})
			.mouseover(show)
			.mouseout(hide)
			.mousemove(update);
		},
		showHTML: function() {
			showHTML(this);
		}
	});
})(jQuery);
