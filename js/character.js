/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Character class
 * @class Character
 * @singleton
 */
Character = (function(c) {
	var Character = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Character.prototype, {
		/**
		 * Initialize character
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.guid = c.guid;
			this.name = c.name;
			this.race = c.race;
			this.klass = c['class'];
			this.level = c.level;
			this.p_faction = (1<<(c.race-1) & racemaskAlliance ? 1 : 0);
			this.guildId = c.guildid;
			this.guildName = c.guildName;
			this.guildRank = c.guildRank;
			
			// call methods
			this.inventory = new Inventory(this);
			this.skills = new Skills(this);
			this.talents = new Talents(this);
			this.quests = new Quests(this);
			this.initBasic();
			this.initQuests();
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function() {
		// header (name, banner color, guild, ...)
			$('#flag').css('background','url(\'images/b_'+this.p_faction+'.jpg\')');
			$('.char_inventory').css("background", 'url(\'images/inventory_'+this.p_faction+'.jpg\')');
			$('#char_name2').html(this.name);
			$('#char_race').html('<img src="images/race_'+this.race+'.png">');
			$('#char_class').html('<img src="images/class_'+this.klass+'.png">');
			$('#char_level').html(this.level);
			if (this.guildId) {
				$('#char_guild_name').html('<a href="guild.html#'+this.guildName+'/basic">'+this.guildName+'</a>');
				$('#char_guild_rank').html('&lt;'+this.guildRank+'&gt;');
			} else {
				$('#char_guild_name').html(L12N('not_in_guild'));
			}
			
			
			// power bar
			$('#power_bar').removeClass();
			if (this.klass == 1) { //rage
				$('#power_name').html('Rage');
				$('#power_bar').addClass('rage_bar');
			} else if (this.klass == 2 || this.klass == 3 || this.klass == 5 || this.klass == 7 || this.klass == 8 || this.klass == 9 || this.klass == 11) { //mana
				$('#power_name').html('Mana');
				$('#power_bar').addClass('mana_bar');
			} else if (this.klass == 4) { //energy
				$('#power_name').html('Energy');
				$('#power_bar').addClass('energy_bar');
			} else if (this.klass == 6) { // runic
				$('#power_name').html('Runic');
				$('#power_bar').addClass('runic_bar');
			}
		},
		
		initQuests: function() {
			var scope = this; 
			var $ul = $('<ul></ul>');
			for (var i=0; i<=4; ++i) {
				$main_zone = $('<li><div style="font-weight: bold">'+L12N('main_zone_'+i)+'</div></li>');
				$ul.append($main_zone);
				
				var $sub_zone = $('<ul></ul>'); 
				$main_zone.append($sub_zone);
				
				for (var j in zones[i]) {
					$zone = $('<li data-value="'+zones[i][j]+'">'+L12N('zone_'+zones[i][j])+'</li>');
					$sub_zone.append($zone);
					$zone.click(function() {
						var zone = $(this).attr('data-value');
						scope.quests.retrieveZone(zone);
					})
				}
				
				
			}
			$ul.treeview();		// 3rd party treeview
			
			$('#quests_categories').append($ul)
		},

		/**
		 * Return character guid
		 * @return Number guid
		 */
		getGuid: function() {
			return this.guid;
		},
		
		/**
		 * Return character class
		 * @return Number class
		 */
		getClass: function() {
			return this.klass;
		},
		
	});

	return Character;
})();

/**
 * Skills class
 * @class Skills
 * @singleton
 */
 
Skills = (function(c) {
	var Skills = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Skills.prototype, {
		/**
		 * Initialize character skills
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.skills = [];
			this.retrieveSkills();
		},

		/**
		 * Get skills from server and render them
		 */
		retrieveSkills: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'skills',
					guid: this.character.getGuid()
				}, function(response) {
					var s = Main.evaluate(response);
					for (var i in s) {
						if (typeof(this.skills[s[i]['grp']]) === "undefined") {
							this.skills[s[i]['grp']] = [];
						}
						this.skills[s[i]['grp']].push(s[i]);
					}
					this.renderSkills();
				},this
			);
		},
		
		/**
		 * Render skills
		 */
		renderSkills: function() {
			var out ='';
			if (this.skills[11]) {
				for (var i=0;i<this.skills[11].length;++i) {
					out += '<div class="tradeskill_wrapper"><div class="tradeskill"><img style="float:left; padding-right: 5px;" src="icons_14/'+this.skills[11][i].icon+'.png"><div style="color: black; float: left;">'+this.skills[11][i].name+'</div><div style="color: #666; float: right;">';
					out += this.skills[11][i].value+' '+' / '+this.skills[11][i].max;
					out += '</div><div class="clear"></div></div></div>';
				}
			}
			$('#tradeskills_recap').html(out);
		}
	});
	
	return Skills;
})();




/**
 * Talents class
 * @class Talents
 * @singleton
 */
 
Talents = (function(c) {
	var Talents = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Talents.prototype, {
		/**
		 * Initialize character talents
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.tabs = {};
			this.talents = {primary: {}, secondary: {}};
			this.glyphs = {};
			this.retrieveTalents();
		},

		/**
		 * Get talents from server and render them
		 */
		retrieveTalents: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'talents',
					guid: this.character.getGuid()
				}, function(response) {
					var s = Main.evaluate(response);

					this.glyphs = s.glyphs;
					
					for (var i in s['tabs']) {
						this.tabs[s['tabs'][i].tree_id] = s['tabs'][i];
					
						this.talents.primary[s['tabs'][i].tree_id] = [];
						this.talents.secondary[s['tabs'][i].tree_id] = [];
					}
					
					//alert(this.talents[851])
					
					for (var i in s['talents']) {
						if (s['talents'][i].spec == 0) {
							this.talents.primary[s['talents'][i].grp].push(s['talents'][i]);
						} else {
							this.talents.secondary[s['talents'][i].grp].push(s['talents'][i]);
						}
					}
					
					this.renderTalents();
					this.renderCalculator();
					this.renderGlyphs();
				},this
			);
		},

		/**
		 * Get talent tree specialization depending on talents distribution
		 * @param {String} build "primary" or "secondary"
		 * @return {Object} talent tree object
		 */
		getCharacterSpec: function(build) {	// primary or secondary
			var topCount = 0;
			var tree = null;
			
			for (var i in this.talents[build]) {
				var count = 0;
				for (var j in this.talents[build][i]) {
					count += parseInt(this.talents[build][i][j].points);
				}
				if (count > topCount) {
					topCount = count;
					tree = this.tabs[i];
				}
			}
			return tree;
		},

		/**
		 * Render talents
		 */
		renderTalents: function() {
			var points = []
			for (var i in this.tabs) {
				points.push(this.talents['primary'][i].length);
			}
			
			var primarySpec = this.getCharacterSpec('primary');
			var secondarySpec = this.getCharacterSpec('secondary');
			if (primarySpec) {
				$('#primary_name').html(talentTabs[primarySpec.tree_id]);
				$('#primary_icon').html('<img src="icons_32/'+(primarySpec.icon)+'.png">');
				$('#primary_points').html(points.join(' / '));
			}
			
			if (secondarySpec) {
				var points = []
				for (var i in this.tabs) {
					points.push(this.talents['secondary'][i].length);
				}
				$('#secondary_name').html(talentTabs[secondarySpec.tree_id]);
				$('#secondary_icon').html('<img src="icons_32/'+(secondarySpec.icon)+'.png">');
				$('#secondary_points').html(points.join(' / '));
			} else {
				$('#secondary_name').html('No spec');
	            $('#secondary_icon').html('<img src="icons_32/inv_misc_questionmark.png">');
	            $('#secondary_points').html('- / - / -');

			}
		},

		_getTierColSpell: function(spec,tab,tier,col) {
			for (var i in this.talents[spec][tab]) {
				if (this.talents[spec][tab][i].tier == tier && this.talents[spec][tab][i].col == col)
					return this.talents[spec][tab][i];
			}
		},
		
		_calculatePrerequisites: function(tabs,icon) {
			for (var i in tabs) {
 				if (tabs[i].id == icon.requires) {
					var tierDiff = icon.tier - tabs[i].tier;
					var colDiff = icon.col - tabs[i].col;
					if (tierDiff == 0 && colDiff == 1) {
						return {name: 'right', css: {left: -21, top: 10}};
					} else if (tierDiff == 0 && colDiff == -1) {
						return {name: 'left', css: {left: 32, top: 10}};
					} else if (tierDiff == 1 && colDiff == 0) {
						return {name: 'down', css: {left: 9, top: -25}};
					} else if (tierDiff == 2 && colDiff == 0) {
						return {name: '2down', css: {left: 9, top: -82}};
					} else if (tierDiff == 1 && colDiff == 1) {
						return {name: 'right_down', css: {left: -21, top: -46}};
					} else {
						return {name: 'unk', css: {left: 0, top: 0}};
					}
				}
			}
		},
		
		renderCalculator: function() {	

			Main.fetch(
				{
					what: 'talents',
					'class': this.character.getClass()
				}, function(response) {
					var tabs = Main.evaluate(response);

					var slots = [];
					for (var i in this.tabs) {
						slots[this.tabs[i].tree_index] = this.tabs[i];
					}

					for (var i in slots) {

						var $wrapper = $('<div style="float: left; padding: 20px 20px 0 0; position: relative;"></div>');

						var slot = slots[i];
						var $name = $('<div style="color: #000">'+talentTabs[slot.tree_id]+'</div>');
						$wrapper.append($name);

						var $slot_wrapper = $('<img src="images/talents_'+this.character.klass+'_'+i+'.jpg">');
						$wrapper.append($slot_wrapper);

						var $tab_wrapper = $('<div style="position: absolute; top: 60px; left: 20px; "></div>');
						$wrapper.append($tab_wrapper);
						for (var j in tabs['tab_'+slot.tree_id]) {
							var icon = tabs['tab_'+slot.tree_id][j];
							var top = 57 * icon.tier;
							var left = 53 * icon.col;

							// get spell (id and points/rank) from character data
							var spell = this._getTierColSpell('primary',slot.tree_id,icon.tier,icon.col);
							// if character doesnt have any points in this talent, use class default to get spell id (for tooltip - rank 1)
							if (!spell) {
								spell = {
									points: null,
									spell: icon.spell
								};
							}

							var $icon_wrapper = $('<div data-type="spell" data-entry="'+spell.spell+'"  style="position: absolute"></div>');
							$icon_wrapper.css({top: top, left: left});

							var $icon = $('<a href="spell.html#'+spell.spell+'"><img style="position: absolute; width:32px; height:32px;" src="icons_32/'+icon.icon+'.png"></a>');
							$icon_wrapper.append($icon);

							if (spell.points) {
								// with points spent in talent - make little window in right bottom corner
								var $points_wrapper = $('<div style="position: relative; top: 25px; left: 25px; background: #000; width: 10px; height: 15px; border: 1px solid #999; border-radius: 3px; line-height: 16px; text-align: center"><span style="color:#'+(spell.points < icon.max ? '1eff00' : 'e6cc80')+'">'+spell.points+'</span></div>');
								$icon_wrapper.append($points_wrapper);
							} else {
								// no points - make it greyscale
								$icon.css('-webkit-filter', 'grayscale(100%)');
							}

							if (icon.requires > 0) {
								var arrow = this._calculatePrerequisites(tabs['tab_'+slot.tree_id],icon);
								var $arrow = $('<img style="position: absolute;" src="images/talent_arrow_'+arrow.name+'.png">');
								$arrow.css(arrow.css);
								$icon_wrapper.append($arrow);
							}
							
							$icon_wrapper.tooltip();
							$tab_wrapper.append($icon_wrapper);
						}
						$('#talents_tree_wrapper').append($wrapper);
					}

				},this
			);
		},
		
		renderGlyphs: function() {
			var $name = $('<div style="color: #000">Prime Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_prime = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_prime);

			var $name = $('<div style="color: #000">Major Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_major = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_major);

			var $name = $('<div style="color: #000">Minor Glyphs</div>');
			$('#talents_glyphs_wrapper').append($name);
			var $ul_minor = $('<ul></ul>');
			$('#talents_glyphs_wrapper').append($ul_minor);

			
			for (var i in this.glyphs['primary']) {
				var $li = $('<li data-type="spell" data-entry="'+this.glyphs['primary'][i].spell_id+'">'+this.glyphs['primary'][i].name+'</li>');
				$li.tooltip();
				if (this.glyphs['primary'][i].glyph_type == 2) {
					$ul_prime.append($li);
				} else if (this.glyphs['primary'][i].glyph_type == 0) {
					$ul_major.append($li);
				} else if (this.glyphs['primary'][i].glyph_type == 1) {
					$ul_minor.append($li);
				} 
			}
			

			
		}
		
	});
	
	return Talents;
})();






/**
* Quests class
* @class Quests
* @singleton
*/

Quests = (function(c) {
	var Quests = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Quests.prototype, {
		/**
		 * Initialize character Quests
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.quests = [];
		},

		/**
		 * Get Quests from server and render them
		 */
		retrieveZone: function(zone) {
			// fetch completed quests by this character in this zone.
			Main.fetch(
				{
					what: 'char',
					action: 'quests',
					guid: this.character.getGuid(),
					zone: zone
				}, function(response) {
					var completedQuests = Main.evaluate(response);

					// Fetch all quests in the zone with their tieles, descriptions, rewards, etc.
					Main.fetch(
						{
							what: 'quests',
							action: 'by_zone',
							zone: zone
						}, function(response) {
							var quests = Main.evaluate(response);
							
							$('#quests_pane').html('');
							
							// use List to render grid of quests, but hide column `Zone` (not necessary - everything in one zone). And add column `Completed` showing yes if quest id presents on completed list
							var list = new List(ListView.templates.quests,quests,{
								hide: ['ZoneOrSort'],
								add: [
								    {
								    	id: 'Completed',
								    	name: 'Completed',
										align: 'center',
										text: function(t,d,s) {
											return search_in_array(d.Id,s.additionalData.quests) ? 'Yes' : '';
										}
								   }
								],
								additionalData: {
									quests: completedQuests
								}
								
							});
							$('#quests_pane').append(list.render());
						},this
					);
				
				},this
			);
		},
	});
	
	return Quests;
})();


