<?php


/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


include "configs/config.php";
include "includes/database.php";
include "includes/cache.php";
include "includes/character.php";
include "includes/search.php";
include "includes/item.php";
include "includes/arena.php";
include "includes/guild.php";
include "includes/quest.php";
include "includes/faction.php";
include "includes/npc.php";
include "includes/spell.php";
include "includes/talents.php";
include "includes/glyph.php";

class Armory {

	public $error_code = 0;
	public $error_text = '';
	public $output = array();	// output before json encoding

	private $_callback = '';	// jsonp callback
	private $_dbh;				// database handler

	private $_guid;
	private $_name;
	private $_race;
	private $_class;
	private $_level;

	function __construct() {

	}

	public function init() {
		$this->_callback = $_GET['callback'];
		$this->db = new Database();
		if ($this->db->error) {
			$this->_throw_error(410,"database error");
		} else {
			switch ($_GET['what']) {
				case 'char':

					$this->character = new Character($this->db,($_GET['guid'] ? $_GET['guid'] : $_GET['name']));
					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $this->character->get_char();
							break;
						case 'inventory':
							$this->_output = $this->character->get_char_items();
							break;
						case 'skills':
							$this->_output = $this->character->get_char_skills();
							break;
						case 'talents':
							$this->_output = $this->character->get_char_talents();
							break;
						case 'quests':	// finished quests in zone
							$this->_output = $this->character->get_completed_quests($_GET['zone']);
							break;
					}
					break;

				case 'search':
					$search = new Search($this->db);
					$this->_output = $search->results($_GET['string']);
					break;

				case 'item':
					$item = new Item($this->db,$_GET['id']);
					$this->_output = $item->get_item();
					break;

				case 'arenateam':
					$arena_team = new Arenateam($this->db,($_GET['id'] ? $_GET['id'] : $_GET['name']));
					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $arena_team->get_team();
							break;
					}
					break;

				case 'guild':
					$guild = new Guild($this->db,($_GET['id'] ? $_GET['id'] : $_GET['name']));
					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $guild->get_guild();
							break;
					}
					break;

				case 'quest':
					$quest = new Quest($this->db,$_GET['id']);
					$this->_output = $quest->get_quest();
					break;

				case 'quests':
					$quests = new Quests($this->db);
					switch ($_GET['action']) {
						case 'by_zone':
							$this->_output = $quests->search_by_zone($_GET['zone']);
							break;
					}
					break;

				case 'npc':
					$npc = new Npc($this->db,$_GET['id']);
					$this->_output = $npc->get_npc();
					break;

				case 'spell':
					$spell = new Spell($this->db,$_GET['id']);
					$this->_output = $spell->get_spell();
					break;

				// get talents template of class (this is being cached as it is not changing - dbc values). In talent calculator it is combined with Character::get_char_talents()
				case 'talents':
					$talents = new Talents($this->db);
					$this->_output = $talents->get_talents_tabs($_GET['class']);
					break;



				
			}
		}

		$this->output();
	}

	private function _throw_error($code,$text) {
		$this->error_code = $code;
		$this->error_text = $text;
	}


	private function output() {
		if ($this->error_code != 0) {
			$error = array(
				'code' => $this->error_code,
				'text' => $this->error_text
			);
			print $this->_callback.'('. json_encode(array('error' => $error)) .');';
		} else {
			$data = array(
				'data' => $this->_output
			);

			print $this->_callback.'('. json_encode($data) .');';
		}
	}
}

error_reporting(0);
header('Content-Type: text/javascript; charset=utf8');

$armory = new Armory();
$armory->init();
